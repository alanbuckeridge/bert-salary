.PHONY: clean data lint requirements sync_data_to_s3 sync_data_from_s3

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
DATA_BUCKET=gs://0157953-thesis/data
MODELS_BUCKET=gs://0157953-thesis/models
LOGS_BUCKET=gs://0157953-thesis/logs
PROJECT_NAME = bert-salary
PYTHON_INTERPRETER = python3
GOOGLE_BERT_MODEL = uncased_L-12_H-768_A-12
GOOGLE_BERT_MODEL_DATE = 2018_10_18
GOOGLE_BERT_MODEL_DIR = models/tf1/google_bert
GOOGLE_BERT_CODE_DIR = src/google_bert
SPLITS_DIR = data/splits
TRAIN_SPLIT = ${SPLITS_DIR}/train.csv
VALID_SPLIT = ${SPLITS_DIR}/valid.csv
TEST_SPLIT = ${SPLITS_DIR}/test.csv
TRAIN_TOKENISED = ${SPLITS_DIR}/train_tokenised.csv
VALID_TOKENISED = ${SPLITS_DIR}/valid_tokenised.csv
TEST_TOKENISED = ${SPLITS_DIR}/test_tokenised.csv
BERT_PYTORCH_TENSORS = data/bert_pytorch_tensors
TRAIN_BERT_INPUTS = ${BERT_PYTORCH_TENSORS}/train_inputs.pt
VALID_BERT_INPUTS = ${BERT_PYTORCH_TENSORS}/valid_inputs.pt
TEST_BERT_INPUTS = ${BERT_PYTORCH_TENSORS}/test_inputs.pt
JOB_PRETRAIN = data/interim/job_pretraining.txt
JOB_PRETRAIN_EVAL = data/interim/job_pretraining_eval.txt
TF_JOB_SAMPLES = data/interim/tf_job_samples.tfrecord
PRETRAINED_TF1_MODEL_DIR = models/tf1/pretrained
PYTORCH_ORIGINAL_BERT_DIR = models/pytorch/untuned/original
PYTORCH_ORIGINAL_BERT = ${PYTORCH_ORIGINAL_BERT_DIR}/pytorch_model.bin
PYTORCH_PRETRAINED_BERT_DIR = models/pytorch/pretrained
PYTORCH_PRETRAINED_BERT = ${PYTORCH_PRETRAINED_BERT_DIR}/pytorch_model.bin
#FINETUNED_ORIGINAL_BERT_DIR = models/pytorch/finetuned/original
#FINETUNED_ORIGINAL_BERT = ${FINETUNED_ORIGINAL_BERT_DIR}/pytorch_model.bin
#FINETUNED_PRETRAINED_BERT_DIR = models/pytorch/finetuned/pretrained
#FINETUNED_PRETRAINED_BERT = ${FINETUNED_PRETRAINED_BERT_DIR}/pytorch_model.bin
FINETUNED_DIR = models/pytorch/finetuned
FINETUNED_FROZEN_ORIGINAL_BERT_DIR = ${FINETUNED_DIR}/original/freeze
FINETUNED_FROZEN_ORIGINAL_BERT = ${FINETUNED_FROZEN_ORIGINAL_BERT_DIR}/pytorch_model.bin
FINETUNED_NONFROZEN_ORIGINAL_BERT_DIR = ${FINETUNED_DIR}/original/full
FINETUNED_NONFROZEN_ORIGINAL_BERT = ${FINETUNED_NONFROZEN_ORIGINAL_BERT_DIR}/pytorch_model.bin
FINETUNED_FROZEN_PRETRAINED_BERT_DIR = ${FINETUNED_DIR}/pretrained/freeze
FINETUNED_FROZEN_PRETRAINED_BERT = ${FINETUNED_FROZEN_PRETRAINED_BERT_DIR}/pytorch_model.bin
FINETUNED_NONFROZEN_PRETRAINED_BERT_DIR = ${FINETUNED_DIR}/pretrained/full
FINETUNED_NONFROZEN_PRETRAINED_BERT = ${FINETUNED_NONFROZEN_PRETRAINED_BERT_DIR}/pytorch_model.bin
VISUALISATIONS_DIR = ../msc-dissertation/images

ifeq (,$(shell which conda))
HAS_CONDA=False
else
HAS_CONDA=True
endif

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Install Python Dependencies
requirements: test_environment
	$(PYTHON_INTERPRETER) -m pip install -U pip setuptools wheel
	$(PYTHON_INTERPRETER) -m pip install -r requirements.txt
	${PYTHON_INTERPRETER} -m spacy download en_core_web_sm


#################################################################################

## Make Dataset
data: requirements ${JOB_PRETRAIN} ${VALID_SPLIT} ${TEST_SPLIT} \
	${TRAIN_TOKENISED} ${VALID_TOKENISED} ${TEST_TOKENISED} \
	${TRAIN_BERT_INPUTS} ${VALID_BERT_INPUTS} ${TEST_BERT_INPUTS}

${JOB_PRETRAIN}: ${TRAIN_SPLIT}
	mkdir -p data/interim
	${PYTHON_INTERPRETER} src/features/create_pretraining_examples.py \
		--include-title \
		--replace-numbers 1 \
		--include-location \
		--include-company \
		--include-category \
		${TRAIN_SPLIT} ${JOB_PRETRAIN}

${JOB_PRETRAIN_EVAL}: ${VALID_SPLIT}
	mkdir -p data/interim
	${PYTHON_INTERPRETER} src/features/create_pretraining_examples.py \
		--include-title \
		--replace-numbers 1 \
		--include-location \
		--include-company \
		--include-category \
		${VALID_SPLIT} ${JOB_PRETRAIN_EVAL}

data/raw/Train_rev1.csv: data/raw/adzuna.zip
	unzip -j -n data/raw/adzuna.zip Train_rev1.csv -d data/raw

${TRAIN_SPLIT} ${VALID_SPLIT} ${TEST_SPLIT}: data/raw/Train_rev1.csv
	mkdir -p data/splits
	$(PYTHON_INTERPRETER) src/data/split_dataset.py data/raw data/splits

%_tokenised.csv: %.csv
	${PYTHON_INTERPRETER} src/features/create_tokenised_text.py \
		--include-title \
		--include-description \
		--include-location \
		--replace-numbers 1 \
		--lemmatise \
		$< $@

${BERT_PYTORCH_TENSORS}/%_inputs.pt: ${SPLITS_DIR}/%.csv ${GOOGLE_BERT_MODEL_DIR}/vocab.txt
	mkdir -p ${BERT_PYTORCH_TENSORS}
	${PYTHON_INTERPRETER} src/features/create_bert_tensors.py \
		--vocab-file-dir ${GOOGLE_BERT_MODEL_DIR} \
		--input-tensors ${BERT_PYTORCH_TENSORS}/$*_inputs.pt \
		--mask-tensors ${BERT_PYTORCH_TENSORS}/$*_masks.pt \
		--salary-tensors ${BERT_PYTORCH_TENSORS}/$*_salaries.pt \
		--include-title \
		--include-description \
		--include-location \
		--replace-numbers 1 \
		--max-length 512 \
		$<

#################################################################################

## Pretrain BERT model on our domain using PyTorch and Huggingface's transformers
pretrain: models/pytorch/pretrained/pytorch_model.bin

models/pytorch/pretrained/pytorch_model.bin: ${JOB_PRETRAIN} src/huggingface/run_language_modeling.py
	mkdir -p models/pytorch/pretrained
	${PYTHON_INTERPRETER} src/huggingface/run_language_modeling.py \
		--output_dir=models/pytorch/pretrained \
		--model_type=bert \
		--model_name_or_path=bert-base-uncased \
		--do_train \
		--train_data_file=${JOB_PRETRAIN} \
		--line_by_line \
		--logging_dir=models/pytorch/pretrained/runs \
		--logging_steps=10000 \
		--save_steps=10000 \
		--save_total_limit=5 \
		--mlm \
		2>&1 | tee logs/pretrain_$$(date -d "today" +"%Y%m%d%H%M").log

src/huggingface/run_language_modeling.py:
	mkdir -p src/huggingface
	wget -P src/huggingface https://raw.githubusercontent.com/huggingface/transformers/v3.0.2/examples/language-modeling/run_language_modeling.py

## Evaluate original BERT model on our domain using PyTorch and Huggingface's transformers
eval_original: ${JOB_PRETRAIN_EVAL} src/huggingface/run_language_modeling.py
	mkdir -p models/pytorch/eval/original
	${PYTHON_INTERPRETER} src/huggingface/run_language_modeling.py \
		--output_dir=models/pytorch/eval/original \
		--model_type=bert \
		--model_name_or_path=bert-base-uncased \
		--do_eval \
		--eval_data_file=${JOB_PRETRAIN_EVAL} \
		--line_by_line \
		--logging_dir=models/pytorch/eval/runs \
		--logging_steps=1000 \
		--mlm \
		2>&1 | tee logs/pretrain_eval_original$$(date -d "today" +"%Y%m%d%H%M").log

## Evaluate pretrained BERT model on our domain using PyTorch and Huggingface's transformers
eval_pretrained: ${JOB_PRETRAIN_EVAL} models/pytorch/pretrained/pytorch_model.bin
	mkdir -p models/pytorch/eval/pretrained
	${PYTHON_INTERPRETER} src/huggingface/run_language_modeling.py \
		--output_dir=models/pytorch/eval/pretrained \
		--model_type=bert \
		--model_name_or_path=models/pytorch/pretrained \
		--do_eval \
		--eval_data_file=${JOB_PRETRAIN_EVAL} \
		--line_by_line \
		--logging_dir=models/pytorch/eval/runs \
		--logging_steps=1000 \
		--mlm \
		2>&1 | tee logs/pretrain_eval_pretrained$$(date -d "today" +"%Y%m%d%H%M").log

#################################################################################

## Pretrain BERT model on our domain (Requires TensorFlow 1)
pretrained_model: data ${PRETRAINED_TF1_MODEL_DIR}/eval_results.txt copy_config_to_pretrained_dir sync_data_to_cloud sync_models_to_cloud

# TODO: target to test environment to ensure TensorFlow 1 for running the Google BERT code.

${PRETRAINED_TF1_MODEL_DIR}/eval_results.txt: ${TF_JOB_SAMPLES} ${GOOGLE_BERT_CODE_DIR}/run_pretraining.py ${GOOGLE_BERT_MODEL_DIR}/bert_config.json
	mkdir -p ${PRETRAINED_TF1_MODEL_DIR}
	${PYTHON_INTERPRETER} ${GOOGLE_BERT_CODE_DIR}/run_pretraining.py \
		--input_file=${TF_JOB_SAMPLES} \
		--output_dir=${PRETRAINED_TF1_MODEL_DIR} \
		--do_train=True \
		--do_eval=True \
		--bert_config_file=${GOOGLE_BERT_MODEL_DIR}/bert_config.json \
		--init_checkpoint=${GOOGLE_BERT_MODEL_DIR}/bert_model.ckpt \
		--train_batch_size=8 \
		--max_seq_length=512 \
		--max_predictions_per_seq=20 \
		--num_train_steps=50000 \
		--num_warmup_steps=10 \
		--learning_rate=2e-5 \
		2>&1 | tee logs/run_pretraining_$$(date -d "today" +"%Y%m%d%H%M").log

copy_config_to_pretrained_dir:
	cp ${GOOGLE_BERT_MODEL_DIR}/bert_config.json ${PRETRAINED_TF1_MODEL_DIR}/config.json
	cp ${GOOGLE_BERT_MODEL_DIR}/vocab.txt ${PRETRAINED_TF1_MODEL_DIR}/vocab.txt

${TF_JOB_SAMPLES}: ${JOB_PRETRAIN} ${GOOGLE_BERT_CODE_DIR}/create_pretraining_data.py ${GOOGLE_BERT_MODEL_DIR}/vocab.txt
	mkdir -p logs
	${PYTHON_INTERPRETER} ${GOOGLE_BERT_CODE_DIR}/create_pretraining_data.py \
		--input_file=${JOB_PRETRAIN} \
		--output_file=${TF_JOB_SAMPLES} \
		--vocab_file=${GOOGLE_BERT_MODEL_DIR}/vocab.txt \
		--do_lower_case=True \
		--max_seq_length=512 \
		--max_predictions_per_seq=20 \
		--masked_lm_prob=0.15 \
		--random_seed=12345 \
		--dupe_factor=10 \
		2>&1 | tee logs/create_pretraining_data_$$(date -d "today" +"%Y%m%d%H%M").log

${GOOGLE_BERT_MODEL_DIR}/${GOOGLE_BERT_MODEL}.zip:
	mkdir -p ${GOOGLE_BERT_MODEL_DIR}
	wget https://storage.googleapis.com/bert_models/${GOOGLE_BERT_MODEL_DATE}/${GOOGLE_BERT_MODEL}.zip -P ${GOOGLE_BERT_MODEL_DIR}/

${GOOGLE_BERT_MODEL_DIR}/bert_config.json ${GOOGLE_BERT_MODEL_DIR}/vocab.txt: ${GOOGLE_BERT_MODEL_DIR}/${GOOGLE_BERT_MODEL}.zip
	unzip -j -n $< -d ${GOOGLE_BERT_MODEL_DIR}

${GOOGLE_BERT_CODE_DIR}/create_pretraining_data.py ${GOOGLE_BERT_CODE_DIR}/run_pretraining.py &:
	mkdir -p ${GOOGLE_BERT_CODE_DIR}
	git clone https://github.com/google-research/bert.git ${GOOGLE_BERT_CODE_DIR}

#################################################################################

## Finetune the PyTorch BERT models on the salary prediction task
finetune: ${FINETUNED_FROZEN_ORIGINAL_BERT} ${FINETUNED_FROZEN_PRETRAINED_BERT} ${FINETUNED_NONFROZEN_ORIGINAL_BERT} ${FINETUNED_NONFROZEN_PRETRAINED_BERT} 

${FINETUNED_FROZEN_ORIGINAL_BERT}: ${TRAIN_SPLIT} ${VALID_SPLIT}
	mkdir -p ${FINETUNED_FROZEN_ORIGINAL_BERT_DIR}
	${PYTHON_INTERPRETER} src/models/finetune_bert_salary.py ${FINETUNED_FROZEN_ORIGINAL_BERT_DIR} \
		--training-data ${TRAIN_SPLIT} \
		--validation-data ${VALID_SPLIT} \
		--include-title \
		--include-description \
		--include-location \
		--replace-numbers \
		--max-length 128 \
		--batch-size 32 \
		--epochs 10 \
		--lr 2e-5 \
		--scaled \
		--freeze \
		2>&1 | tee logs/finetuning_original_frozen_$$(date -d "today" +"%Y%m%d%H%M").log

${FINETUNED_FROZEN_PRETRAINED_BERT}: ${PYTORCH_PRETRAINED_BERT} ${TRAIN_SPLIT} ${VALID_SPLIT}
	mkdir -p ${FINETUNED_FROZEN_PRETRAINED_BERT_DIR}
	${PYTHON_INTERPRETER} src/models/finetune_bert_salary.py ${FINETUNED_FROZEN_PRETRAINED_BERT_DIR} ${PYTORCH_PRETRAINED_BERT_DIR} \
		--training-data ${TRAIN_SPLIT} \
		--validation-data ${VALID_SPLIT} \
		--include-title \
		--include-description \
		--include-location \
		--replace-numbers \
		--max-length 128 \
		--batch-size 32 \
		--epochs 10 \
		--lr 2e-5 \
		--scaled \
		--freeze \
		2>&1 | tee logs/finetuning_pretrained_frozen_$$(date -d "today" +"%Y%m%d%H%M").log

${FINETUNED_NONFROZEN_ORIGINAL_BERT}: ${TRAIN_SPLIT} ${VALID_SPLIT}
	mkdir -p ${FINETUNED_NONFROZEN_ORIGINAL_BERT_DIR}
	${PYTHON_INTERPRETER} src/models/finetune_bert_salary.py ${FINETUNED_NONFROZEN_ORIGINAL_BERT_DIR} \
		--training-data ${TRAIN_SPLIT} \
		--validation-data ${VALID_SPLIT} \
		--include-title \
		--include-description \
		--include-location \
		--replace-numbers \
		--max-length 128 \
		--batch-size 32 \
		--epochs 10 \
		--lr 2e-5 \
		--scaled \
		2>&1 | tee logs/finetuning_original_full_$$(date -d "today" +"%Y%m%d%H%M").log

${FINETUNED_NONFROZEN_PRETRAINED_BERT}: ${PYTORCH_PRETRAINED_BERT} ${TRAIN_SPLIT} ${VALID_SPLIT}
	mkdir -p ${FINETUNED_NONFROZEN_PRETRAINED_BERT_DIR}
	${PYTHON_INTERPRETER} src/models/finetune_bert_salary.py ${FINETUNED_NONFROZEN_PRETRAINED_BERT_DIR} ${PYTORCH_PRETRAINED_BERT_DIR} \
		--training-data ${TRAIN_SPLIT} \
		--validation-data ${VALID_SPLIT} \
		--include-title \
		--include-description \
		--include-location \
		--replace-numbers \
		--max-length 128 \
		--batch-size 32 \
		--epochs 10 \
		--lr 2e-5 \
		--scaled \
		2>&1 | tee logs/finetuning_pretrained_full_$$(date -d "today" +"%Y%m%d%H%M").log

#################################################################################

## Evaluate sklearn RandomForestRegressor on the training and final test sets
evaluate_rfr: ${TRAIN_TOKENISED} ${TEST_TOKENISED} src/models/test_rfr.py
	${PYTHON_INTERPRETER} src/models/test_rfr.py ${TRAIN_TOKENISED} ${TEST_TOKENISED} \
		--n-estimators 250 \
		2>&1 | tee logs/eval_rfr_$$(date -d "today" +"%Y%m%d%H%M").log

#################################################################################

## Evaluate the finetuned BERT models on the training and final test sets
evaluate_bert: eval_finetuned_bert_original_frozen eval_finetuned_bert_pretrained_frozen eval_finetuned_bert_original_full eval_finetuned_bert_pretrained_full

eval_finetuned_bert_original_frozen: ${TRAIN_SPLIT} ${TEST_SPLIT} ${FINETUNED_FROZEN_ORIGINAL_BERT} src/models/test_bert.py
	${PYTHON_INTERPRETER} src/models/test_bert.py ${FINETUNED_FROZEN_ORIGINAL_BERT_DIR} \
		--training-data ${TRAIN_SPLIT} \
		--test-data ${TEST_SPLIT} \
		--include-title \
		--include-description \
		--include-location \
		--replace-numbers \
		--max-length 128 \
		--batch-size 32 \
		2>&1 | tee logs/eval_bert_original_frozen_$$(date -d "today" +"%Y%m%d%H%M").log

eval_finetuned_bert_pretrained_frozen: ${TRAIN_SPLIT} ${TEST_SPLIT} ${FINETUNED_FROZEN_PRETRAINED_BERT} src/models/test_bert.py
	${PYTHON_INTERPRETER} src/models/test_bert.py ${FINETUNED_FROZEN_PRETRAINED_BERT_DIR} \
		--training-data ${TRAIN_SPLIT} \
		--test-data ${TEST_SPLIT} \
		--include-title \
		--include-description \
		--include-location \
		--replace-numbers \
		--max-length 128 \
		--batch-size 32 \
		2>&1 | tee logs/eval_bert_pretrained_frozen_$$(date -d "today" +"%Y%m%d%H%M").log

eval_finetuned_bert_original_full: ${TRAIN_SPLIT} ${TEST_SPLIT} ${FINETUNED_NONFROZEN_ORIGINAL_BERT} src/models/test_bert.py
	${PYTHON_INTERPRETER} src/models/test_bert.py ${FINETUNED_NONFROZEN_ORIGINAL_BERT_DIR} \
		--training-data ${TRAIN_SPLIT} \
		--test-data ${TEST_SPLIT} \
		--include-title \
		--include-description \
		--include-location \
		--replace-numbers \
		--max-length 128 \
		--batch-size 32 \
		2>&1 | tee logs/eval_bert_original_full_$$(date -d "today" +"%Y%m%d%H%M").log

eval_finetuned_bert_pretrained_full: ${TRAIN_SPLIT} ${TEST_SPLIT} ${FINETUNED_NONFROZEN_PRETRAINED_BERT} src/models/test_bert.py
	${PYTHON_INTERPRETER} src/models/test_bert.py ${FINETUNED_NONFROZEN_PRETRAINED_BERT_DIR} \
		--training-data ${TRAIN_SPLIT} \
		--test-data ${TEST_SPLIT} \
		--include-title \
		--include-description \
		--include-location \
		--replace-numbers \
		--max-length 128 \
		--batch-size 32 \
		2>&1 | tee logs/eval_bert_pretrained_full_$$(date -d "today" +"%Y%m%d%H%M").log

#################################################################################

## Output some statistics about the data to a file
stats: data/stats/stats.txt data/stats/ngrams.txt

data/stats/stats.txt: ${TRAIN_SPLIT} src/stats/make_stats.py
	mkdir -p data/stats
	${PYTHON_INTERPRETER} src/stats/make_stats.py ${TRAIN_SPLIT} data/stats/stats.txt

data/stats/ngrams.txt: ${TRAIN_SPLIT} src/stats/ngrams.py
	mkdir -p data/stats
	${PYTHON_INTERPRETER} src/stats/ngrams.py ${TRAIN_SPLIT} data/stats/ngrams.txt

#################################################################################

## Assess different hyperparameters on scikit-learn algorithms
param_search: ${TRAIN_TOKENISED} ${VALID_TOKENISED} src/models/sklearn_paramsearch.py
	mkdir -p logs
	${PYTHON_INTERPRETER} src/models/sklearn_paramsearch.py \
		--knn \
		--rfr \
		${TRAIN_TOKENISED} ${VALID_TOKENISED}  \
		2>&1 | tee logs/run_gridsearch_$$(date -d "today" +"%Y%m%d%H%M").log

#################################################################################

## Create graphs/visualisations etc.
graphs: ${TRAIN_SPLIT}
	mkdir -p ${VISUALISATIONS_DIR}
	${PYTHON_INTERPRETER} src/visualization/visualize.py \
		${TRAIN_SPLIT} \
		${VISUALISATIONS_DIR} \
		logs/finetuning_original_frozen_202007150028.log \
		logs/finetuning_original_full_202007150443.log \
		logs/finetuning_pretrained_frozen_202007150235.log \
		logs/finetuning_pretrained_full_202007151107.log

#################################################################################

## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete

## Lint using flake8
lint:
	flake8 src

## Upload Data to Cloud Storage
sync_data_to_cloud:
	gsutil -m rsync -r data ${DATA_BUCKET}

## Download Data from Cloud Storage
sync_data_from_cloud:
	mkdir -p data
	gsutil -m rsync -r ${DATA_BUCKET} data

## Upload Models to Cloud Storage
sync_models_to_cloud:
	gsutil -m rsync -r models ${MODELS_BUCKET}

## Download Models from Cloud Storage
sync_models_from_cloud:
	mkdir -p models
	gsutil -m rsync -r ${MODELS_BUCKET} models

## Upload logs to Cloud Storage
sync_logs_to_cloud:
	gsutil -m rsync -r logs ${LOGS_BUCKET}

## Download logs from Cloud Storage
sync_logs_from_cloud:
	mkdir -p logs
	gsutil -m rsync -r ${LOGS_BUCKET} logs

## Set up python interpreter environment (Anaconda)
create_environment:
ifeq (True,$(HAS_CONDA))
	@echo ">>> Detected conda, creating conda environment."
	conda create --yes --name $(PROJECT_NAME) python=3.8 pip
else
	@echo ">>> Anaconda not detected, cannot create conda environment."
	$(error Install Anaconda and try again.)
endif

## Need to add a kernel to ipython so it can be selected in Jupyter
setup_jupyter:
	ipython kernel install --user --name=$(PROJECT_NAME)-kernel

## Test python environment is setup correctly
test_environment:
	$(PYTHON_INTERPRETER) test_environment.py

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
