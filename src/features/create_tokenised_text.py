# -*- coding: utf-8 -*-
import click
import csv
import logging
import os
import re
import spacy
from tqdm import tqdm

logger = logging.getLogger(os.path.basename(__file__))


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.argument('output_file', type=click.File('w'))
@click.option('--include-title', is_flag=True)
@click.option('--include-description', is_flag=True)
@click.option('--include-location', is_flag=True)
@click.option('--include-company', is_flag=True)
@click.option('--include-category', is_flag=True)
@click.option('--lemmatise', is_flag=True)
@click.option('--replace-numbers')
def main(input_file, output_file, include_title, include_location, include_description,
         include_company, include_category, lemmatise, replace_numbers):
    """
    Converts the Adzuna job data into a format suitable for BERT pretraining.
    This will be fed to the create_pretraining_data.py script.

    The output will consist of one sentence per line.
    "Documents" (job posts) are delimited with empty lines.
    """

    logger.info(f'Tokenising {input_file.name}...')

    lines = [line for line in input_file]
    nlp = spacy.load('en_core_web_sm', disable=['token', 'parser', 'ner'])

    logger.info(f'Writing to {output_file.name}...')
    csvwriter = csv.writer(output_file)

    for row in tqdm(csv.reader(lines), total=len(lines)):
        assert len(row) == 12

        # Data format fields:
        # Id,Title,FullDescription,LocationRaw,LocationNormalized,ContractType,ContractTime,Company,Category,SalaryRaw,SalaryNormalized,SourceName
        _, title, desc, locRaw, locNorm, contractType, contractTime, company, category, salaryRaw, salary, source = row

        if replace_numbers:
            desc = re.sub(r'\*{2,}', replace_numbers, desc)
            if include_title and title:
                title = re.sub(r'\*{2,}', replace_numbers, title)

        fields = []

        if include_title and title:
            fields.append(tokenise(title, nlp, lemmatise))
        if include_description and desc:
            fields.append(tokenise(desc, nlp, lemmatise))
        if include_location and locRaw:
            fields.append(tokenise(locRaw, nlp, lemmatise))
        if include_company and company:
            fields.append(tokenise(company, nlp, lemmatise))
        if include_category and category:
            fields.append(tokenise(category, nlp, lemmatise))

        text = ' '.join(fields)

        csvwriter.writerow([text, salary])

    logger.info(f'Tokenised text written to {output_file.name}.')


def tokenise(text, nlp, lemmatise=False):
    doc = nlp(text)
    if lemmatise:
        tokens = [t.lemma_ for t in doc]
    else:
        tokens = [t.text for t in doc]
    return ' '.join(tokens)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
