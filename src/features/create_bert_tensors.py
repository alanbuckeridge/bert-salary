# -*- coding: utf-8 -*-
import click
import csv
import logging
import os
import re
import torch
from tqdm import tqdm
from transformers import BertTokenizer

logger = logging.getLogger(os.path.basename(__file__))


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('--input-tensors', required=True, help='File to write PyTorch tensors for input token IDs')
@click.option('--mask-tensors', required=True, help='File to write PyTorch tensors for attention masks')
@click.option('--salary-tensors', required=True, help='File to write PyTorch tensors for salaries (target)')
@click.option('--vocab-file-dir', required=True, help='Directory containing vocab.txt')
@click.option('--include-title', is_flag=True)
@click.option('--include-description', is_flag=True)
@click.option('--include-location', is_flag=True)
@click.option('--include-company', is_flag=True)
@click.option('--include-category', is_flag=True)
@click.option('--max-length', type=int, help='Maximum sequence length (up to 512)')
@click.option('--replace-numbers')
def main(input_file, input_tensors, mask_tensors, salary_tensors, vocab_file_dir,
         include_title, include_location, include_description,
         include_company, include_category, max_length, replace_numbers):
    """
    Uses BERT tokeniser to tokenise the text in input_file, converts it to three
    PyTorch tensors:
    1. Input Ids (token IDs including special [CLS] and [SEP] tokens)
    2. Attention masks (0 for padding tokens, 1 for other tokens)
    3. Salaries (i.e. the target variable)
    and outputs them to Pickle files
    """

    logger.info(f'BERT Tokenising {input_file.name}...')

    tokenizer = BertTokenizer.from_pretrained(vocab_file_dir)
    lines = [line for line in input_file]

    input_ids = []
    attention_masks = []
    salaries = []

    for row in tqdm(csv.reader(lines), total=len(lines)):
        assert len(row) == 12

        # Data format fields:
        # Id,Title,FullDescription,LocationRaw,LocationNormalized,ContractType,ContractTime,Company,Category,SalaryRaw,SalaryNormalized,SourceName
        _, title, desc, locRaw, locNorm, contractType, contractTime, company, category, salaryRaw, salary, source = row
        salaries.append(float(salary))

        if replace_numbers:
            desc = re.sub(r'\*{2,}', replace_numbers, desc)
            if include_title and title:
                title = re.sub(r'\*{2,}', replace_numbers, title)

        fields = []

        if include_title and title:
            fields.append(title)
        if include_description and desc:
            fields.append(desc)
        if include_location and locRaw:
            fields.append(locRaw)
        if include_company and company:
            fields.append(company)
        if include_category and category:
            fields.append(category)

        text = ' '.join(fields)

        # `encode_plus` will:
        #  1. Convert the text to lower-case
        #  2. Tokenize the sentence
        #  3. Prepend the `[CLS]` token and append the `[SEP]` token
        #  4. Return a dict containing 3 int lists:
        #      i. List of token ids
        #     ii. List of token-type IDs; 0 for first sequence, 1 for second sequence
        #           (it'll be all 0s for us as we provide only one sequence)
        #    iii. List of attention-mask values: 0 for padding tokens, 1 for other tokens
        encoded_text = tokenizer.encode_plus(text,
                                             add_special_tokens=True,
                                             pad_to_max_length=True,
                                             max_length=max_length)
        input_ids.append(encoded_text['input_ids'])
        attention_masks.append(encoded_text['attention_mask'])

    # Convert all inputs and labels into torch tensors, the required datatype
    # for BERT
    logger.info(f'Writing input tensors to {input_tensors}...')
    torch.save(torch.tensor(input_ids), input_tensors)
    logger.info(f'Writing mask tensors to {mask_tensors}...')
    torch.save(torch.tensor(attention_masks), mask_tensors)
    logger.info(f'Writing salary tensors to {salary_tensors}...')
    torch.save(torch.tensor(salaries), salary_tensors)

    logger.info(f'Finished with {input_file.name}.')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
