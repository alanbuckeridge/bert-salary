# -*- coding: utf-8 -*-
import click
import csv
import logging
import os
import re
import spacy
from tqdm import tqdm

logger = logging.getLogger(os.path.basename(__file__))


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.argument('output_file', type=click.File('w'))
@click.option('--include-title', is_flag=True)
@click.option('--include-location', is_flag=True)
@click.option('--include-company', is_flag=True)
@click.option('--include-category', is_flag=True)
@click.option('--replace-numbers')
def main(input_file, output_file, include_title, include_location,
         include_company, include_category, replace_numbers):
    """
    Converts the Adzuna job data into a format suitable for BERT pretraining.
    This will be fed to the create_pretraining_data.py script.

    The output will consist of one sentence per line.
    "Documents" (job posts) are delimited with empty lines.
    """

    logger.info(f'Preparing {input_file.name} for BERT pretraining...')
    logger.info('Reading...')

    lines = [line for line in input_file]

    logger.info('Processing...')
    documents = []
    nlp = spacy.load('en_core_web_sm', disable=['token', 'ner'])

    for row in tqdm(csv.reader(lines), total=len(lines)):
        assert len(row) == 12

        _, title, desc, location, _, _, _, company, category, _, _, _ = row

        sentences = []

        if replace_numbers:
            desc = re.sub(r'\*{2,}', replace_numbers, desc)
            if include_title and title:
                title = re.sub(r'\*{2,}', replace_numbers, title)

        if include_title and title:
            sentences.append(title)
        if include_location and location:
            sentences.append(location)
        if include_company and company:
            sentences.append(company)
        if include_category and category:
            sentences.append(category)

        for sent in nlp(desc).sents:
            sentences.append(sent.text)

        documents.append('\n'.join(sentences))

    # Write out all documents, separated by empty lines.
    output_file.write('\n\n'.join(documents))
    output_file.write('\n')

    logger.info(f'Pretraining samples written to {output_file.name}.')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
