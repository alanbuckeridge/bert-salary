# -*- coding: utf-8 -*-
import click
import logging
import numpy as np
import os
import pandas as pd
import re
import string
import spacy
import tqdm
from collections import Counter
from nltk import ngrams

PERCENTILES = [0, 25, 50, 75, 90, 95, 99, 100]
NUMBER_OF_NGRAMS = 50

logger = logging.getLogger(os.path.basename(__file__))
nlp = spacy.load('en_core_web_sm', disable=['token', 'parser', 'ner'])


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.argument('output_file', type=click.File('w'))
def produce_ngram_stats(input_file, output_file):

    logger.info(f'Producing ngram stats for text in {input_file.name}...')
    logger.info(f'Writing ngram stats to {output_file.name}...')

    df = pd.read_csv(input_file,
                     header=None,
                     names=['Id', 'Title', 'FullDescription', 'LocationRaw',
                            'LocationNormalized', 'ContractType',
                            'ContractTime', 'Company', 'Category',
                            'SalaryRaw', 'SalaryNormalized', 'SourceName'])

    # Title
    logger.info('Writing ngram stats for Title...')
    titles = np.array(df.Title.values)
    write_ngram_stats("Title",
                      get_ngram_stats(titles),
                      output_file)

    # FullDescription
    logger.info('Writing ngram stats for FullDescription...')
    descriptions = np.array(df.FullDescription.values)
    write_ngram_stats("FullDescription",
                      get_ngram_stats(descriptions),
                      output_file)

    logger.info("Finished writing ngram stats.")


def get_ngrams(raw_input):
    """
    Returns unigrams, bigrams, and trigrams for the given input.
    Input should be a string.
    The ngrams are returned as lists of strings.
    """

    # Adzuna replaced numbers with '****'. Replace those with a single digit
    text = re.sub(r'\*{2,}', '1', raw_input)
    doc = nlp(text)
    unigrams = [str.lower(t.text.strip())
                for t in doc
                if t.text.strip() and t.text.strip() not in string.punctuation]
    bigrams = [' '.join(n) for n in ngrams(unigrams, 2)]
    trigrams = [' '.join(n) for n in ngrams(unigrams, 3)]
    return unigrams, bigrams, trigrams


def get_ngram_stats(text_field):
    """
    text_field should be an array-like collection of strings (e.g. a "column"
    in a Pandas dataframe, or a numpy array)

    Returns a bunch of stats on the text in that field
    """
    unigram_counter = Counter()
    bigram_counter = Counter()
    trigram_counter = Counter()
    lengths = []
    missing = 0
    longest = 0
    long_texts = []

    for text in tqdm.tqdm(text_field):
        if not text:
            missing += 1
        else:
            unigrams, bigrams, trigrams = get_ngrams(text)
            lengths.append(len(unigrams))
            unigram_counter.update(unigrams)
            bigram_counter.update(bigrams)
            trigram_counter.update(trigrams)
            if len(unigrams) == longest:
                long_texts.append(text)
            elif len(unigrams) > longest:
                longest = len(unigrams)
                long_texts = [text]

    stats = dict()
    stats['unigrams'] = unigram_counter
    stats['bigrams'] = bigram_counter
    stats['trigrams'] = trigram_counter
    stats['no_of_docs'] = len(lengths)
    stats['min'] = np.min(lengths)
    stats['max'] = np.max(lengths)
    stats['mean'] = np.mean(lengths)
    stats['median'] = np.median(lengths)
    stats['std'] = np.std(lengths)
    stats['percentiles'] = np.percentile(lengths, PERCENTILES)
    stats['missing'] = missing
    stats['long_texts'] = long_texts

    return stats


def write_ngram_stats(name, stats, output_file):
    insert_header(name, output_file)
    output_file.write("Missing values: {}\n".format(stats['missing']))
    output_file.write('\n')

    output_file.write("Longest text:\n")
    for t in stats['long_texts']:
        output_file.write('- {}\n'.format(t))
    output_file.write('\n')

    output_file.write("Word count\n")
    output_file.write("----------\n")
    output_file.write("Max: {}\n".format(stats['max']))
    output_file.write("Min: {}\n".format(stats['min']))
    output_file.write("Mean: {}\n".format(stats['mean']))
    output_file.write("Median: {}\n".format(stats['median']))
    output_file.write("Std: {}\n".format(stats['std']))
    output_file.write("Percentiles:\n")
    for percentile, value in zip(PERCENTILES, stats['percentiles']):
        output_file.write("- {}% : {}\n".format(percentile, value))
    output_file.write('\n')

    for g in ['unigrams', 'bigrams', 'trigrams']:
        counter = stats[g]
        output_file.write(str.title(g) + '\n')
        output_file.write('-' * len(g) + '\n')
        output_file.write('Unique: {}\n'.format(len(counter)))
        output_file.write('Most common:\n')
        for gram, n in counter.most_common(NUMBER_OF_NGRAMS):
            output_file.write('{:6}: {}\n'.format(n, gram))
        output_file.write('\n')


def insert_header(name, output_file):
    output_file.write(name + '\n')
    output_file.write('=' * len(name) + '\n')
    output_file.write('\n')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    produce_ngram_stats()
