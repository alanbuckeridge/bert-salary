# -*- coding: utf-8 -*-
import click
import logging
import numpy as np
import os
import pandas as pd
from collections import Counter

logger = logging.getLogger(os.path.basename(__file__))


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.argument('output_file', type=click.File('w'))
def produce_stats(input_file, output_file):

    logger.info(f'Producing stats for data in {input_file.name}...')
    logger.info(f'Writing stats to {output_file.name}...')

    df = pd.read_csv(input_file,
                     header=None,
                     names=['Id', 'Title', 'FullDescription', 'LocationRaw',
                            'LocationNormalized', 'ContractType',
                            'ContractTime', 'Company', 'Category',
                            'SalaryRaw', 'SalaryNormalized', 'SourceName'])

    # Title
    logger.info('Writing stats for Title...')
    insert_header("Title", output_file)
    output_file.write(str(df.Title.describe()) + '\n')
    output_file.write('\n')
    titles = np.array(df.Title.values)
    write_top_n(titles, output_file)
    write_salary_stats('Title', df, output_file)

    # LocationRaw
    logger.info('Writing stats for LocationRaw...')
    insert_header("LocationRaw", output_file)
    output_file.write(str(df.LocationRaw.describe()) + '\n')
    output_file.write('\n')
    write_top_n(df.LocationRaw.values, output_file)
    write_salary_stats('LocationRaw', df, output_file)

    # LocationNormalized
    logger.info('Writing stats for LocationNormalized...')
    insert_header("LocationNormalized", output_file)
    output_file.write(str(df.LocationNormalized.describe()) + '\n')
    output_file.write('\n')
    write_top_n(df.LocationNormalized.values, output_file)
    write_salary_stats('LocationNormalized', df, output_file)

    # ContractType
    logger.info('Writing stats for ContractType...')
    insert_header("ContractType", output_file)
    output_file.write(str(df.ContractType.value_counts(dropna=False)) + '\n')
    output_file.write('\n')
    write_salary_stats('ContractType', df, output_file)

    # ContractTime
    logger.info('Writing stats for ContractTime...')
    insert_header("ContractTime", output_file)
    output_file.write(str(df.ContractTime.value_counts(dropna=False)) + '\n')
    output_file.write('\n')
    write_salary_stats('ContractTime', df, output_file)

    # Company
    logger.info('Writing stats for Company...')
    insert_header("Company", output_file)
    output_file.write(str(df.Company.describe()) + '\n')
    output_file.write('\n')
    write_top_n(df.Company.values, output_file)
    write_salary_stats('Company', df, output_file)

    # Category
    logger.info('Writing stats for Category...')
    insert_header("Category", output_file)
    output_file.write(str(df.Category.describe()) + '\n')
    output_file.write('\n')
    write_top_n(df.Category.values, output_file)
    write_salary_stats('Category', df, output_file, n=30)

    # SourceName
    logger.info('Writing stats for SourceName...')
    insert_header("SourceName", output_file)
    output_file.write(str(df.SourceName.describe()) + '\n')
    output_file.write('\n')
    write_top_n(df.SourceName.values, output_file)
    write_salary_stats('SourceName', df, output_file)

    # SalaryRaw
    logger.info('Writing stats for SalaryRaw...')
    insert_header("SalaryRaw", output_file)
    output_file.write(str(df.SalaryRaw.describe()) + '\n')
    output_file.write('\n')
    write_top_n(df.SalaryRaw.values, output_file)

    # SalaryNormalized
    logger.info('Writing stats for SalaryNormalized...')
    insert_header("SalaryNormalized", output_file)
    fmt = "{:10} {:14.6f}\n"
    salaries = np.array(df.SalaryNormalized.values)
    output_file.write(fmt.format('Count', len(salaries)))
    output_file.write(fmt.format('Mean', np.mean(salaries)))
    output_file.write(fmt.format('Std', np.std(salaries)))
    output_file.write(fmt.format('Min', np.min(salaries)))
    percentiles = [25, 50, 75, 90, 95, 99]
    for i, p in enumerate(np.percentile(salaries, percentiles)):
        output_file.write('{:2}% {:21.6f}\n'.format(percentiles[i], p))
    output_file.write(fmt.format('Max', np.max(salaries)))

    logger.info('Finished writing stats.')


def insert_header(name, output_file):
    output_file.write(name + '\n')
    output_file.write('=' * len(name) + '\n')
    output_file.write('\n')


def write_top_n(text, output_file, n=50):
    header = "Top " + str(n)
    output_file.write(header + '\n')
    output_file.write('-' * len(header) + '\n')
    for value, count in Counter(text).most_common(n):
        output_file.write("{:6d} : {}\n".format(count, value))
    output_file.write('\n\n')


def write_salary_stats(name, dataframe, output_file, n=20):
    by_group = dataframe.groupby(name)['SalaryNormalized'].agg(
        [np.size, np.sum, np.min, np.max, np.mean])

    for stat in ['size', 'sum', 'amin', 'amax', 'mean']:
        output_file.write(str(by_group[[stat]].sort_values(
            by=stat, ascending=(stat == 'amin'))
                              .head(n)) + '\n\n')
    output_file.write('\n')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    produce_stats()
