# -*- coding: utf-8 -*-
import click
import logging
import numpy as np
import os
import pandas as pd
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

INTERIM_FILENAME = "Train_rev1.csv"
TRAIN_FILENAME = "train.csv"
VALIDATION_FILENAME = "valid.csv"
TEST_FILENAME = "test.csv"

logger = logging.getLogger(os.path.basename(__file__))


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path(exists=True))
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to split unzipped CSV data from (data/raw)
        into training, validation, and test sets (saved in data/splits).
    """

    logger.info('Splitting data into train, test, and validation sets')

    # Set seed to make this reproducible
    np.random.seed(0)

    all = pd.read_csv(input_filepath + '/' + INTERIM_FILENAME)

    mask = np.random.rand(len(all)) < 0.8
    test_df = all[~mask]
    train_and_valid = all[mask]
    mask = np.random.rand(len(train_and_valid)) < 0.9
    train_df = train_and_valid[mask]
    valid_df = train_and_valid[~mask]

    logger.info('Writing train, test, and validation data to CSV')
    write_df_as_csv(train_df, output_filepath, TRAIN_FILENAME)
    write_df_as_csv(valid_df, output_filepath, VALIDATION_FILENAME)
    write_df_as_csv(test_df, output_filepath, TEST_FILENAME)


def write_df_as_csv(df, output_filepath, filename):
    f = Path(output_filepath + '/' + filename)
    if f.exists():
        logger.info('{} already exists. Will not overwrite'.format(f.name))
    else:
        logger.info('Writing {}'.format(f.name))
        df.to_csv(f, header=False, index=False)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
