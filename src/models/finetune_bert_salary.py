import logging
import math
import os
import random
import time

import click
import numpy as np
import torch
from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.preprocessing import StandardScaler
from torch.utils.data import DataLoader
from tqdm import tqdm
from transformers import BertTokenizer, BertForSequenceClassification, AdamW, get_linear_schedule_with_warmup

from src.models.bert_for_salary_prediction_model import BertForSalaryPrediction
from src.models.salary_dataset import SalaryDataset
from src.models.utils import get_device, format_time

logger = logging.getLogger(os.path.basename(__file__))


@click.command()
@click.argument('output_dir', type=click.Path(exists=True))
@click.argument('model_dir', type=click.Path(exists=True), required=False)
@click.option('--training-data', required=True, help='CSV file containing training data set')
@click.option('--validation-data', required=True, help='CSV file containing validation data set')
@click.option('--include-title', is_flag=True)
@click.option('--include-description', is_flag=True)
@click.option('--include-location', is_flag=True)
@click.option('--include-company', is_flag=True)
@click.option('--include-category', is_flag=True)
@click.option('--replace-numbers', is_flag=True)
@click.option('--max-length', type=int, help='Maximum sequence length (up to 512)')
@click.option('--batch-size', required=True, type=int, help='Batch size')
@click.option('--lr', type=float, help='Adam learning rate', default=2e-5)
@click.option('--eps', type=float, help='Adam epsilon', default=1e-8)
@click.option('--epochs', type=int, help='Epochs', default=4)
@click.option('--freeze', is_flag=True, help='"Freeze" the BERT layers and just train the regression head')
@click.option('--sqrt', is_flag=True, help='Transform salaries to their square roots before training and validation')
@click.option('--scaled', is_flag=True, help='Transform salaries using StandardScaler before training and validation')
@click.option('--custom', is_flag=True, help='Use our custom Bert model instead of default BertForSequenceClassification')
def main(output_dir, model_dir, training_data, validation_data,
         include_title, include_location, include_description,
         include_company, include_category, replace_numbers,
         max_length, freeze, sqrt, scaled, custom, batch_size,
         lr=2e-5, eps=1e-8, epochs=4):

    log_options(output_dir, model_dir, training_data, validation_data,
                include_title, include_location, include_description,
                include_company, include_category, replace_numbers,
                max_length, freeze, sqrt, scaled, custom, batch_size,
                lr, eps, epochs)

    if scaled and sqrt:
        raise ValueError("Only one of --scaled and --sqrt may be specified")

    logger.info("Starting...")
    logger.info("")

    device = get_device(logger)

    # ========================================
    #       Load the BERT model
    # ========================================

    if model_dir:
       # Load the BERT model that we pretrained on job ad text
        model_name_or_path = model_dir
    else:
        # Load the vanilla BERT-base that hasn't been pretrained on the job ad text
        model_name_or_path = "bert-base-uncased"

    if custom:
        model = BertForSalaryPrediction.from_pretrained(
            model_name_or_path,
            num_labels=1,                # Regression
            output_attentions=False,     # don't return attentions weights.
            output_hidden_states=False,  # don't return all hidden-states.
            freeze_bert=freeze,          # Whether to 'freeze' the BERT layers
        )
    else:
        model = BertForSequenceClassification.from_pretrained(
            model_name_or_path,
            num_labels=1,                # Regression
            output_attentions=False,     # don't return attentions weights.
            output_hidden_states=False,  # don't return all hidden-states.
        )
        if freeze:
            for param in model.bert.parameters():
                param.requires_grad = False

    model.cuda(device)

    # ========================================
    #    Load training and validation data
    # ========================================

    if scaled:
        scaler = StandardScaler()
    elif sqrt:
        scaler = SqrtScaler()
    else:
        scaler = NoOpScaler()

    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
    train_set = SalaryDataset(training_data, tokenizer, maxlen=max_length, include_title=include_title,
                              include_description=include_description, include_location=include_location,
                              include_company=include_company, include_category=include_category,
                              replace_numbers=replace_numbers)
    scaler.fit(np.array(train_set.salaries).reshape(-1, 1))
    train_set.salaries = scaler.transform(np.array(train_set.salaries).reshape(-1, 1)).flatten()
    train_dataloader = DataLoader(train_set, batch_size=batch_size, num_workers=5)
    valid_set = SalaryDataset(validation_data, tokenizer, maxlen=max_length, include_title=include_title,
                              include_description=include_description, include_location=include_location,
                              include_company=include_company, include_category=include_category,
                              replace_numbers=replace_numbers)
    valid_set.salaries = scaler.transform(np.array(valid_set.salaries).reshape(-1, 1)).flatten()
    valid_dataloader = DataLoader(valid_set, batch_size=batch_size, num_workers=5)

    # ========================================
    #       Training and Validation loop
    # ========================================

    # Set the seed value all over the place to make this reproducible.
    seed_val = 0
    random.seed(seed_val)
    np.random.seed(seed_val)
    torch.manual_seed(seed_val)
    torch.cuda.manual_seed_all(seed_val)

    # To reproduce the training procedure from the BERT paper, we'll use the
    # AdamW optimizer provided by Hugging Face. It corrects weight decay, so
    # it's similar to the original paper. We'll also use a linear scheduler
    # with no warmup steps:
    optimizer = AdamW(model.parameters(), lr=lr, eps=eps, correct_bias=False)
    total_steps = len(train_dataloader) * epochs
    scheduler = get_linear_schedule_with_warmup(
        optimizer,
        num_warmup_steps=0,
        num_training_steps=total_steps)

    # Store the average training and validation losses after each epoch so we can plot them.
    loss_values = []
    validation_loss_values = []
    for epoch_i in range(0, epochs):
        logger.info("")
        logger.info('======== Epoch {:} / {:} ========'.format(epoch_i + 1, epochs))
        logger.info('Training...')

        train_loss = train(model, train_dataloader, device, optimizer, scheduler)
        loss_values.append(train_loss)

        validation_loss = validate(model, valid_dataloader, device, scaler)
        validation_loss_values.append(validation_loss)

    # ========================================
    #       Save fine-tuned model
    # ========================================

    logger.info("")
    logger.info("Training complete!")
    logger.info("Training losses: " + str(loss_values))
    logger.info("Validation losses: " + str(validation_loss_values))

    logger.info("")
    logger.info(f"Saving model to {output_dir}...")
    model_to_save = model.module if hasattr(model, 'module') else model  # Take care of distributed/parallel training
    model_to_save.save_pretrained(output_dir)

    logger.info("")
    logger.info("Finished!")


def train(model, train_dataloader, device, optimizer, scheduler):
    # ========================================
    #               Training
    # ========================================

    # Perform one full pass over the training set.

    # Measure how long the training epoch takes.
    t0 = time.time()

    # Reset the total loss for this epoch.
    total_loss = 0

    # Put the model into training mode. Don't be mislead--the call to
    # `train` just changes the *mode*, it doesn't *perform* the training.
    # `dropout` and `batchnorm` layers behave differently during training
    # vs. test (source: https://stackoverflow.com/questions/51433378/what-does-model-train-do-in-pytorch)
    model.train()

    # For each batch of training data...
    for batch in tqdm(train_dataloader):

        # Unpack this training batch from our dataloader.
        #
        # As we unpack the batch, we'll also copy each tensor to the GPU using the
        # `to` method.
        #
        # `batch` contains three pytorch tensors:
        #   [0]: input ids
        #   [1]: attention masks
        #   [2]: targets
        #
        # For some reason, the input_ids and masks are 3d rather than 
        # the required 2d, so they need to be reshaped
        batch_size = len(batch[2])
        b_input_ids = batch[0].reshape(batch_size, -1)
        b_input_mask = batch[1].reshape(batch_size, -1)
        b_targets = batch[2]
        b_input_ids = b_input_ids.to(device)
        b_input_mask = b_input_mask.to(device)
        b_targets = b_targets.to(device)

        # Always clear any previously calculated gradients before performing a
        # backward pass. PyTorch doesn't do this automatically because
        # accumulating the gradients is "convenient while training RNNs".
        # (source: https://stackoverflow.com/questions/48001598/why-do-we-need-to-call-zero-grad-in-pytorch)
        model.zero_grad()

        # Perform a forward pass (evaluate the model on this training batch).
        # The documentation for this `model` function is here:
        # https://huggingface.co/transformers/v2.2.0/model_doc/bert.html#transformers.BertForSequenceClassification
        outputs = model(b_input_ids,
                        token_type_ids=None,
                        attention_mask=b_input_mask,
                        labels=b_targets)

        # The call to `model` always returns a tuple, so we need to pull the
        # loss value out of the tuple.
        loss = outputs[0]

        # Accumulate the training loss over all of the batches so that we can
        # calculate the average loss at the end. `loss` is a Tensor containing a
        # single value; the `.item()` function just returns the Python value
        # from the tensor.
        total_loss += loss.item()

        # Perform a backward pass to calculate the gradients.
        loss.backward()

        # Clip the norm of the gradients to 1.0.
        # This is to help prevent the "exploding gradients" problem.
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)

        # Update parameters and take a step using the computed gradient.
        # The optimizer dictates the "update rule"--how the parameters are
        # modified based on their gradients, the learning rate, etc.
        optimizer.step()

        # Update the learning rate.
        scheduler.step()

    # Calculate the average loss over the training data.
    avg_train_loss = total_loss / len(train_dataloader)

    logger.info("")
    logger.info("  Average training loss (MSE): {0:.2f}".format(avg_train_loss))
    logger.info("  Average training loss (RMSE): {0:.2f}".format(math.sqrt(avg_train_loss)))
    logger.info("  Training epoch took: {:}".format(format_time(time.time() - t0)))

    return avg_train_loss


def validate(model, validation_dataloader, device, scaler):
    # ========================================
    #               Validation
    # ========================================
    # After the completion of each training epoch, measure our performance on
    # our validation set.

    logger.info("")
    logger.info("Running Validation...")

    t0 = time.time()

    # Put the model in evaluation mode--the dropout layers behave differently
    # during evaluation.
    model.eval()

    # Tracking variables
    salaries = np.array([])
    predictions = np.array([])

    # Evaluate data for one epoch
    for batch in tqdm(validation_dataloader):
        # Add inputs and masks to GPU, but not targets
        batch_size = len(batch[2])
        b_input_ids = batch[0].reshape(batch_size, -1).to(device)
        b_input_mask = batch[1].reshape(batch_size, -1).to(device)
        targets = batch[2].numpy()

        # Telling the model not to compute or store gradients, saving memory and
        # speeding up validation
        with torch.no_grad():
            # Forward pass, calculate predictions.
            outputs = model(b_input_ids,
                            token_type_ids=None,
                            attention_mask=b_input_mask)

        # Get the "logits" output by the model.
        logits = outputs[0]

        # Move logits and labels to CPU
        logits = logits.detach().cpu().numpy()

        # De-scale logits and targets if necessary
        if scaler:
            logits = scaler.inverse_transform(logits)
            targets = scaler.inverse_transform(targets)

        salaries = np.append(salaries, targets.flatten())
        predictions = np.append(predictions, logits.flatten())

    # Report the final performance for this validation run.
    mae = mean_absolute_error(salaries, predictions)
    mse = mean_squared_error(salaries, predictions)
    rmse = math.sqrt(mse)
    logger.info("  MAE:  {0:.2f}".format(mae))
    logger.info("  MSE:  {0:.2f}".format(mse))
    logger.info("  RMSE: {0:.2f}".format(rmse))
    logger.info("  Validation took: {:}".format(format_time(time.time() - t0)))

    return mae


def log_options(output_dir, model_dir, training_data, validation_data,
                include_title, include_location, include_description,
                include_company, include_category, replace_numbers,
                max_length, freeze, sqrt, scaled, custom, batch_size,
                lr, eps, epochs):
    logger.info("Chosen Options:")
    logger.info("===============")
    logger.info(f"model_dir: {model_dir}")
    logger.info(f"output_dir: {output_dir}")
    logger.info(f"training_data: {training_data}")
    logger.info(f"validation_data: {validation_data}")
    logger.info(f"include_title: {include_title}")
    logger.info(f"include_description: {include_description}")
    logger.info(f"include_location: {include_location}")
    logger.info(f"include_company: {include_company}")
    logger.info(f"include_category: {include_category}")
    logger.info(f"replace_numbers: {replace_numbers}")
    logger.info(f"max_length: {max_length}")
    logger.info(f"freeze: {freeze}")
    logger.info(f"sqrt: {sqrt}")
    logger.info(f"scaled: {scaled}")
    logger.info(f"custom: {custom}")
    logger.info(f"batch_size: {batch_size}")
    logger.info(f"epochs: {epochs}")
    logger.info(f"eps: {eps}")
    logger.info(f"lr: {lr}")
    logger.info("")



class SqrtScaler:
    """
    Implements the sklearn Scaler interface (like StandardScaler)

    This transforms values into their square roots
    """

    def fit(self, X):
        return self

    def transform(self, X):
        return np.sqrt(X)

    def fit_transform(self, X):
        return self.transform(X)

    def inverse_transform(self, X):
        return np.power(X, 2)


class NoOpScaler:
    """
    Implements the sklearn Scaler interface (like StandardScaler)

    This does not change the values, i.e. no-op
    """

    def fit(self, X):
        return self

    def transform(self, X):
        return X

    def fit_transform(self, X):
        return self.transform(X)

    def inverse_transform(self, X):
        return X


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
