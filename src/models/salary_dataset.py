import csv
import re
import torch
import numpy as np
from torch.utils.data import Dataset
from tqdm import tqdm


class SalaryDataset(Dataset):
    """
    Allows PyTorch to process the data in an Adzuna dataset.

    Based on code written by Kabir Ahuja
    (https://github.com/kabirahuja2431/FineTuneBERT/blob/master/src/dataloader.py)
    """

    def __init__(self, input_file, tokenizer, maxlen=512, include_title=False, include_description=True,
                 include_location=False, include_company=False, include_category=False, replace_numbers=True):

        self.tokenizer = tokenizer
        self.maxlen = maxlen
        self.include_title = include_title
        self.include_description = include_description
        self.include_location = include_location
        self.include_company = include_company
        self.include_category = include_category
        self.replace_numbers = replace_numbers
        self.texts, self.salaries = self._init_rows(input_file)

    def _init_rows(self, input_file):
        """
        Converts each line of the CSV into a tuple of (text, salary)
        """
        with open(input_file) as f:
            lines = f.readlines()

        texts = list()
        salaries = list()

        for row in tqdm(csv.reader(lines), total=len(lines)):
            assert len(row) == 12

            # Data format fields:
            # Id,Title,FullDescription,LocationRaw,LocationNormalized,ContractType,ContractTime,Company,Category,SalaryRaw,SalaryNormalized,SourceName
            _, title, desc, loc_raw, loc_norm, contract_type, contract_time, company, category, salary_raw, salary, source = row

            # Adzuna has replaced all numbers in the text with the string '****'.
            # BERT tokenizer treats each '*' as a separate token.
            # The following code converts such strings to a single digit
            if self.replace_numbers:
                if self.include_description and desc:
                    desc = re.sub(r'\*{2,}', '1', desc)
                if self.include_title and title:
                    title = re.sub(r'\*{2,}', '1', title)

            fields = []

            if self.include_title and title:
                fields.append(title)
            if self.include_description and desc:
                fields.append(desc)
            if self.include_location and loc_raw:
                fields.append(loc_raw)
            if self.include_company and company:
                fields.append(company)
            if self.include_category and category:
                fields.append(category)

            text = ' '.join(fields)

            texts.append(text)
            salaries.append(float(salary))

        return texts, salaries

    def __len__(self):
        return len(self.texts)

    def __getitem__(self, index):

        # `encode_plus` will:
        #  1. Convert the text to lower-case
        #  2. Tokenize the sentence
        #  3. Prepend the `[CLS]` token and append the `[SEP]` token
        #  4. Return a dict containing 3 int lists:
        #      i. List of token ids
        #     ii. List of token-type IDs; 0 for first sequence, 1 for second sequence
        #           (it'll be all 0s for us as we provide only one sequence)
        #    iii. List of attention-mask values: 0 for padding tokens, 1 for other tokens
        encoded_text = self.tokenizer.encode_plus(self.texts[index],
                                                  add_special_tokens=True,
                                                  pad_to_max_length=True,
                                                  max_length=self.maxlen,
                                                  truncation=True,
                                                  return_token_type_ids=False,
                                                  return_attention_mask=True,
                                                  return_tensors='pt')

        return encoded_text['input_ids'], encoded_text['attention_mask'], torch.tensor(self.salaries[index])
