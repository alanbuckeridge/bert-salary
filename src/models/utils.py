import datetime

import torch


def get_device(logger):
    """
    :return: The PyTorch 'device' to use (i.e. GPU if available, CPU otherwise )
    """
    if torch.cuda.is_available():
        device = torch.device("cuda")
        logger.info('Number of GPUs available: %d' % torch.cuda.device_count())
        logger.info('We will use the GPU: ' + torch.cuda.get_device_name(0))
    else:
        logger.info('No GPU available, using the CPU instead. (Prepare to wait!)')
        device = torch.device("cpu")

    return device


def format_time(elapsed):
    """
    Takes a time in seconds and returns a string hh:mm:ss
    """
    # Round to the nearest second.
    elapsed_rounded = int(round(elapsed))

    # Format as hh:mm:ss
    return str(datetime.timedelta(seconds=elapsed_rounded))
