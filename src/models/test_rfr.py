import logging
import math
import os

import click
import pandas as pd
from sklearn.decomposition import TruncatedSVD
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.pipeline import Pipeline

logger = logging.getLogger(os.path.basename(__file__))


@click.command()
@click.argument('train_file', type=click.File('r'))
@click.argument('test_file', type=click.File('r'))
@click.option('--n-estimators', type=int, help='Number of estimators', default=250)
def main(train_file, test_file, n_estimators):
    """
    Train and test a RandomForestRegressor on the Adzuna job ad data

    TRAIN_FILE and TEST_FILE are CSVs with two fields: text and salary.

    Results are logged to stdout
    """
    logger.info("Starting...")

    train_df = pd.read_csv(train_file, header=None, names=['text', 'salary'])
    test_df = pd.read_csv(test_file, header=None, names=['text', 'salary'])

    pipeline = Pipeline([
        ('tfidf', TfidfVectorizer(lowercase=True,
                                  ngram_range=(1, 3),
                                  min_df=10,
                                  max_df=0.25)),
        ('svd', TruncatedSVD(n_components=300)),
        ('rfr', RandomForestRegressor(verbose=10, n_jobs=-1, random_state=0, n_estimators=n_estimators))
    ])

    logger.info("Training...")
    pipeline.fit(train_df.text, train_df.salary)

    logger.info("\nTesting on Training set...")
    mae, mse, rmse = evaluate(pipeline, train_df)
    report(mae, mse, rmse)

    logger.info("\nTesting on Test set...")
    mae, mse, rmse = evaluate(pipeline, test_df)
    report(mae, mse, rmse)


def evaluate(pipeline, df):
    y_pred = pipeline.predict(df.text)
    mae = mean_absolute_error(df.salary, y_pred)
    mse = mean_squared_error(df.salary, y_pred)
    rmse = math.sqrt(mse)
    return mae, mse, rmse


def report(mae, mse, rmse):
    logger.info("  MAE:  {0:.2f}".format(mae))
    logger.info("  MSE:  {0:.2f}".format(mse))
    logger.info("  RMSE: {0:.2f}".format(rmse))


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
