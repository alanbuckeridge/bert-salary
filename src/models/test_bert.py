import logging
import math
import os
import random
import time

import click
import numpy as np
import torch
from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.preprocessing import StandardScaler
from torch.utils.data import DataLoader
from tqdm import tqdm
from transformers import BertTokenizer, BertForSequenceClassification

from src.models.salary_dataset import SalaryDataset
from src.models.utils import get_device, format_time

logger = logging.getLogger(os.path.basename(__file__))

@click.command()
@click.argument('model_dir', type=click.Path(exists=True), required=False)
@click.option('--training-data', required=True, help='CSV file containing training data set')
@click.option('--test-data', required=True, help='CSV file containing test data set')
@click.option('--include-title', is_flag=True)
@click.option('--include-description', is_flag=True)
@click.option('--include-location', is_flag=True)
@click.option('--include-company', is_flag=True)
@click.option('--include-category', is_flag=True)
@click.option('--replace-numbers', is_flag=True)
@click.option('--max-length', type=int, help='Maximum sequence length (up to 512)')
@click.option('--batch-size', required=True, type=int, help='Batch size')
def main(model_dir, training_data, test_data,
         include_title, include_location, include_description,
         include_company, include_category, replace_numbers,
         max_length, batch_size):

    log_options(model_dir, training_data, test_data,
                include_title, include_location, include_description,
                include_company, include_category, replace_numbers,
                max_length, batch_size)

    logger.info("Starting...\n")

    device = get_device(logger)

    # ========================================
    #       Load the BERT model
    # ========================================

    model = BertForSequenceClassification.from_pretrained(
        model_dir,
        num_labels=1,                # Regression
        output_attentions=False,     # don't return attentions weights.
        output_hidden_states=False,  # don't return all hidden-states.
    )
    model.cuda(device)

    # ========================================
    #    Load training and test data
    # ========================================

    scaler = StandardScaler()

    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
    train_set = SalaryDataset(training_data, tokenizer, maxlen=max_length, include_title=include_title,
                              include_description=include_description, include_location=include_location,
                              include_company=include_company, include_category=include_category,
                              replace_numbers=replace_numbers)
    scaler.fit(np.array(train_set.salaries).reshape(-1, 1))
    train_set.salaries = scaler.transform(np.array(train_set.salaries).reshape(-1, 1)).flatten()
    train_dataloader = DataLoader(train_set, batch_size=batch_size, num_workers=5)
    test_set = SalaryDataset(test_data, tokenizer, maxlen=max_length, include_title=include_title,
                              include_description=include_description, include_location=include_location,
                              include_company=include_company, include_category=include_category,
                              replace_numbers=replace_numbers)
    test_set.salaries = scaler.transform(np.array(test_set.salaries).reshape(-1, 1)).flatten()
    test_dataloader = DataLoader(test_set, batch_size=batch_size, num_workers=5)

    # ========================================
    #             Testing loop
    # ========================================

    # Set the seed value all over the place to make this reproducible.
    seed_val = 0
    random.seed(seed_val)
    np.random.seed(seed_val)
    torch.manual_seed(seed_val)
    torch.cuda.manual_seed_all(seed_val)


    logger.info("Evaluating model on training data set...")
    train_mae, train_mse, train_rmse = evaluate(model, train_dataloader, device, scaler)
    logger.info("  MAE:  {0:.2f}".format(train_mae))
    logger.info("  MSE:  {0:.2f}".format(train_mse))
    logger.info("  RMSE: {0:.2f}".format(train_rmse))

    logger.info("\nEvaluating model on test data set...")
    test_mae, test_mse, test_rmse = evaluate(model, test_dataloader, device, scaler)
    logger.info("  MAE:  {0:.2f}".format(test_mae))
    logger.info("  MSE:  {0:.2f}".format(test_mse))
    logger.info("  RMSE: {0:.2f}".format(test_rmse))

    logger.info("\nEvaluation complete!")


def evaluate(model, test_dataloader, device, scaler):

    t0 = time.time()

    # Put the model in evaluation mode
    model.eval()

    # Tracking variables
    salaries = np.array([])
    predictions = np.array([])

    # Evaluate data for one epoch
    for batch in tqdm(test_dataloader):
        # Unpack this test batch from our dataloader.
        #
        # `batch` contains three pytorch tensors:
        #   [0]: input ids
        #   [1]: attention masks
        #   [2]: targets
        #
        # As we unpack the batch, we'll also copy the input and mask tensors
        # (but not the targets) to the GPU using the `to` method.
        #
        # For some reason, the input_ids and masks are 3d rather than
        # the required 2d, so they need to be reshaped
        batch_size = len(batch[2])
        b_input_ids = batch[0].reshape(batch_size, -1).to(device)
        b_input_mask = batch[1].reshape(batch_size, -1).to(device)
        targets = batch[2].numpy()

        # Telling the model not to compute or store gradients, saving memory and
        # speeding up validation
        with torch.no_grad():
            # Forward pass, calculate predictions.
            outputs = model(b_input_ids,
                            token_type_ids=None,
                            attention_mask=b_input_mask)

        # Get the "logits" output by the model.
        logits = outputs[0]

        # Move logits and labels to CPU
        logits = logits.detach().cpu().numpy()

        # un-scale logits and targets
        logits = scaler.inverse_transform(logits)
        targets = scaler.inverse_transform(targets)

        salaries = np.append(salaries, targets.flatten())
        predictions = np.append(predictions, logits.flatten())

    logger.info("  Evaluation took: {:}".format(format_time(time.time() - t0)))

    # Report the final performance for this validation run.
    mae = mean_absolute_error(salaries, predictions)
    mse = mean_squared_error(salaries, predictions)
    rmse = math.sqrt(mse)

    return mae, mse, rmse


def log_options(model_dir, training_data, test_data,
                include_title, include_location, include_description,
                include_company, include_category, replace_numbers,
                max_length, batch_size):
    logger.info("Chosen Options:")
    logger.info("===============")
    logger.info(f"model_dir: {model_dir}")
    logger.info(f"training_data: {training_data}")
    logger.info(f"test_data: {test_data}")
    logger.info(f"include_title: {include_title}")
    logger.info(f"include_description: {include_description}")
    logger.info(f"include_location: {include_location}")
    logger.info(f"include_company: {include_company}")
    logger.info(f"include_category: {include_category}")
    logger.info(f"replace_numbers: {replace_numbers}")
    logger.info(f"max_length: {max_length}")
    logger.info(f"batch_size: {batch_size}")
    logger.info("")


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
