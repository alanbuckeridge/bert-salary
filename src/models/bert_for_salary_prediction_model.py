from transformers import BertPreTrainedModel, BertModel
from torch import nn
from torch.nn import MSELoss


class BertForSalaryPrediction(BertPreTrainedModel):
    def __init__(self, config, regression_hidden_layers=300, freeze_bert=False):
        super().__init__(config)

        self.bert = BertModel(config)
        if freeze_bert:
            for param in self.bert.parameters():
                param.requires_grad = False

        self.dropout = nn.Dropout(config.hidden_dropout_prob)
        self.hidden1 = nn.Linear(config.hidden_size, regression_hidden_layers)
        self.activation = nn.Tanh()
        self.hidden2 = nn.Linear(regression_hidden_layers, 1)

        self.init_weights()

    def forward(
            self,
            input_ids=None,
            attention_mask=None,
            token_type_ids=None,
            position_ids=None,
            head_mask=None,
            inputs_embeds=None,
            labels=None,
            output_attentions=None,
            output_hidden_states=None,
    ):

        outputs = self.bert(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
        )

        X = outputs[1]
        X = self.dropout(X)
        X = self.hidden1(X)
        X = self.activation(X)
        logits = self.hidden2(X)

        outputs = (logits,) + outputs[2:]  # add hidden states and attention if they are here

        if labels is not None:
            loss_fct = MSELoss()
            loss = loss_fct(logits.view(-1), labels.view(-1))
            outputs = (loss,) + outputs

        return outputs  # (loss), logits, (hidden_states), (attentions)

