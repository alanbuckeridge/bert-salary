import logging
import os

import click
import pandas as pd
from sklearn.decomposition import TruncatedSVD
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import mean_absolute_error
from sklearn.neighbors import KNeighborsRegressor
from sklearn.pipeline import Pipeline

logger = logging.getLogger(os.path.basename(__file__))
preprocess_steps = [
    ('tfidf', TfidfVectorizer(lowercase=True,
                              ngram_range=(1, 3),
                              min_df=10,
                              max_df=0.25)),
    ('svd', TruncatedSVD(n_components=300))
]


@click.command()
@click.argument('train_file', type=click.File('r'))
@click.argument('valid_file', type=click.File('r'))
@click.option('--rfr', is_flag=True, help="Perform grid search for RandomForestRegressor")
@click.option('--knn', is_flag=True, help="Perform grid search for KNeighborsRegressor")
def main(train_file, valid_file, rfr, knn):
    """
    Do GridSearchCV on the data in TRAIN_FILE, and validate the best
    parameters on VALID_FILE.

    TRAIN_FILE and VALID_FILE are CSVs with two fields: text and salary.

    Results are logged to stdout
    """
    logger.info("Starting...")

    train_df = pd.read_csv(train_file, header=None, names=['text', 'salary'])
    valid_df = pd.read_csv(valid_file, header=None, names=['text', 'salary'])

    if rfr:
        search_rfr(train_df, valid_df)
    if knn:
        search_knn(train_df, valid_df)


def search_rfr(train_df, valid_df):
    """Search for best number of estimators for RandomForestRegressor"""

    regressors = [(f'rfr_{n}', RandomForestRegressor(verbose=10, n_jobs=-1, random_state=0, n_estimators=n))
                  for n in [50, 100, 250]]
    do_search(train_df, valid_df, regressors)


def search_knn(train_df, valid_df):
    """Search for best number of neighbours for KNeighborsRegressor"""

    regressors = [(f'knn_{k}', KNeighborsRegressor(n_jobs=-1, n_neighbors=k)) for k in [5, 10, 20]]
    do_search(train_df, valid_df, regressors)


def do_search(train_df, valid_df, regressors):
    """Regressors is a list of tuples: ('name', Regressor)"""

    best_mae = None
    best_regressor = None

    for name, regressor in regressors:
        pipeline = Pipeline(preprocess_steps + [(name, regressor)])
        logger.info("\nRunning search for " + str(pipeline))
        logger.info("Training...")
        pipeline.fit(train_df.text, train_df.salary)
        logger.info("Testing on validation set...")
        y_pred = pipeline.predict(valid_df.text)
        mae = mean_absolute_error(valid_df.salary, y_pred)
        logger.info("Mean Absolute Error: %0.3f" % mae)
        if best_mae is None or mae < best_mae:
            best_mae = mae
            best_regressor = regressor

    logger.info(f"\nBest MAE = {best_mae} for regressor = {best_regressor}")


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
