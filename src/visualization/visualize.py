import click
import logging
import matplotlib.pyplot as plt
import os
import pandas as pd
import re


logger = logging.getLogger(os.path.basename(__file__))


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.argument('output_dir', type=click.Path(exists=True))
@click.argument('finetune_original_frozen', type=click.File('r'))
@click.argument('finetune_original_full', type=click.File('r'))
@click.argument('finetune_pretrained_frozen', type=click.File('r'))
@click.argument('finetune_pretrained_full', type=click.File('r'))
def visualise(input_file, output_dir, finetune_original_frozen,
              finetune_original_full, finetune_pretrained_frozen,
              finetune_pretrained_full):

    df = pd.read_csv(input_file,
                     header=None,
                     names=['Id', 'Title', 'FullDescription', 'LocationRaw',
                            'LocationNormalized', 'ContractType',
                            'ContractTime', 'Company', 'Category',
                            'SalaryRaw', 'SalaryNormalized', 'SourceName'])

    raw_salary_histogram(df.SalaryNormalized.values, output_dir)
    raw_salary_boxplot(df.SalaryNormalized.values, output_dir)

    orig_frozen_train_losses, orig_frozen_valid_losses = get_finetune_losses(
        finetune_original_frozen)
    orig_full_train_losses, orig_full_valid_losses = get_finetune_losses(
        finetune_original_full)
    pretrain_frozen_train_losses, pretrain_frozen_valid_losses = get_finetune_losses(
        finetune_pretrained_frozen)
    pretrain_full_train_losses, pretrain_full_valid_losses = get_finetune_losses(
        finetune_pretrained_full)

    plot_losses(orig_frozen_train_losses, orig_full_train_losses,
                pretrain_frozen_train_losses, pretrain_full_train_losses,
                output_dir, 'training-losses', 'Mean Squared Error')
    plot_losses(orig_frozen_valid_losses, orig_full_valid_losses,
                pretrain_frozen_valid_losses, pretrain_full_valid_losses,
                output_dir, 'validation-losses', 'Mean Absolute Error (£)')


def raw_salary_histogram(salaries, output_dir):
    filename = output_dir + '/raw-salary-histogram.pdf'
    logger.info(filename)
    f = plt.figure()
    plt.hist(salaries, bins=50)
    plt.xlabel("(£)")
    f.savefig(filename, bbox_inches='tight')


def raw_salary_boxplot(salaries, output_dir):
    filename = output_dir + '/raw-salary-boxplot.pdf'
    logger.info(filename)
    f = plt.figure()
    plt.boxplot(salaries)
    # plt.xticks([])
    plt.ylabel("(£)")
    plt.xlabel("SalaryNormalized")
    f.savefig(filename, bbox_inches='tight')


def get_finetune_losses(logfile):
    logger.info(f"Reading {logfile.name}...")
    training_losses = []
    validation_losses = []

    for line in logfile:
        if "Training losses" in line:
            training_losses = [float(x) for x in re.findall(r'\d+\.\d+', line)]
        elif "Validation losses" in line:
            validation_losses = [float(x) for x in re.findall(r'\d+\.\d+', line)]

    return training_losses, validation_losses


def plot_losses(orig_frozen_losses, orig_full_losses, pretrain_frozen_losses,
                pretrain_full_losses, output_dir, name, ylabel):
    filename = output_dir + '/' + name + '.pdf'
    logger.info(filename)
    x = list(range(1, 11))
    f = plt.figure()
    plt.plot(x, orig_frozen_losses, label="BERT Base, frozen")
    plt.plot(x, pretrain_frozen_losses, label="BERT Jobs, frozen")
    plt.plot(x, orig_full_losses, label="BERT Base, full")
    plt.plot(x, pretrain_full_losses, label="BERT Jobs, full")
    plt.xlabel("Epochs")
    plt.ylabel(ylabel)
    plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.9))

    f.savefig(filename, bbox_inches='tight')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    visualise()
