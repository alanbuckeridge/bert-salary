from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Using BERT to predict salaries from job ads. For MSc in AI',
    author='Alan Buckeridge',
    license='',
)
